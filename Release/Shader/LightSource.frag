#version 330 core

out vec4 fragmentColor;

uniform vec3 lightSourceColor;
uniform float gamma;

void main() {
	vec4 result =  vec4(lightSourceColor, 1.0f);

	result.rgb = pow(result.rgb, vec3(1.0f/gamma));

	fragmentColor = result;
}