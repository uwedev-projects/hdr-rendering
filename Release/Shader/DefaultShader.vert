#version 330 core

layout (location = 0) in vec3 position;
layout (location = 1) in vec3 normal;
layout (location = 2) in vec2 texturePosition;
layout (location = 3) in vec3 tangent;

struct PointLight {
	vec3 position;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;

	float constant;
	float linear;
	float quadratic;

	float farPlane;

	samplerCube shadowCubeMap;
};

#define MAX_POINT_LIGHTS 1

uniform PointLight pointLights[MAX_POINT_LIGHTS];
uniform vec3 viewPosition;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform mat4 lightSpace;
uniform mat3 normalMatrix;

out vec3 fragPos;
out vec3 fragNormal;
out vec4 fragLightPos;
out vec2 fragTexPos;
out vec3 viewPos;

out vec3 tanFragPos;
out vec3 tanViewPos;
out vec3 tanPointLightPositions[MAX_POINT_LIGHTS];

void main() {
	gl_Position = projection * view * model * vec4(position, 1.0f);
	fragPos = vec3(model * vec4(position, 1.0f));
	fragLightPos = vec4(lightSpace * model * vec4(position, 1.0f));
	fragNormal = normalize(normalMatrix *  normal);
	fragTexPos = vec2(texturePosition.x, texturePosition.y);

	vec3 T = normalize(normalMatrix * tangent);
	vec3 N = fragNormal;
	vec3 B = normalize(cross(N, T));

	mat3 TBN = transpose(mat3(T, B, N));

	tanFragPos = TBN * fragPos;
	tanViewPos = TBN * viewPosition;

	for(int i = 0; i < MAX_POINT_LIGHTS; ++i) {
		tanPointLightPositions[i] = TBN * pointLights[i].position;
	}
}