#version 330 core

layout (location = 0) in vec3 position;
layout (location = 2) in vec2 texPos;

out vec2 fragTexPos;

void main()
{
	gl_Position = vec4(position, 1.0f);
	fragTexPos = vec2(texPos.x, texPos.y);
}