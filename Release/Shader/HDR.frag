#version 330 core

in vec2 fragTexPos;

out vec4 fragColor;

#define PP_NONE			0x0000
#define PP_INVERSION	0x0001
#define PP_GREYSCALE	0x0002
#define PP_BOXBLUR		0x0003
#define PP_SHARPEN		0x0004
#define PP_EDGE_DETECT	0x0005

uniform float gamma;
uniform float exposure;
uniform int useGammaCorrection;
uniform int postProcessingMode;
uniform sampler2D hdrColorBuffer;

vec3 toneMapping(vec3 hdrColor);
vec3 correctGamma(vec3 color);

vec3 invert(vec3 color);
vec3 greyScale(vec3 color);
vec3 boxBlur(vec2 texCoords);
vec3 sharpen(vec2 texCoords);
vec3 edgeDetect(vec2 texCoords);

void main() 
{
	vec3 color = texture(hdrColorBuffer, fragTexPos).rgb;

	switch(postProcessingMode) {
		case PP_INVERSION:
			color = invert(color);
			break;
		case PP_GREYSCALE:
			color = greyScale(color);
			break;
		case PP_BOXBLUR:
			color = boxBlur(fragTexPos);
			break;
		case PP_SHARPEN:
			color = sharpen(fragTexPos);
			break;
		case PP_EDGE_DETECT:
			color = edgeDetect(fragTexPos);
			break;
	};

	color = toneMapping(color);

	if(useGammaCorrection == 1)
		color = correctGamma(color);

	fragColor = vec4(color, 1.0f);
}

vec3 toneMapping(vec3 hdrColor)
{
//	vec3 mapped = hdrColor / vec3(hdrColor + 1.0f);
	vec3 mapped = vec3(1.0f) - exp(-hdrColor * exposure);
	return mapped;
}

vec3 correctGamma(vec3 color) 
{
	return pow(color, vec3( 1.0f / gamma ));
}


const float kernelOffset = 1.0f / 300.0f;
const vec2 kernelCoords[9] = vec2[] (
	vec2(-kernelOffset, kernelOffset),
	vec2(0.0f, kernelOffset),
	vec2(kernelOffset, kernelOffset),
	vec2(-kernelOffset, 0.0f),
	vec2(0.0f, 0.0f),
	vec2(kernelOffset, 0.0f),
	vec2(-kernelOffset, -kernelOffset),
	vec2(0.0f, -kernelOffset),
	vec2(kernelOffset, -kernelOffset)
);

vec3 invert(vec3 color) {
	return vec3(1.0f) - color;
};

vec3 greyScale(vec3 color) {
	float average = 0.2126f * color.r + 0.7152f * color.g + 0.0722f * color.b;
	return vec3(average, average, average);
};

const float[9] blurKernel = float[] (
	1.0f / 9.0f, 1.0f / 9.0f, 1.0f / 9.0f,
	1.0f / 9.0f, 1.0f / 9.0f, 1.0f / 9.0f,
	1.0f / 9.0f, 1.0f / 9.0f, 1.0f / 9.0f
);

vec3 boxBlur(vec2 texCoords) {
	vec3 texSamples[9];
	for(int i = 0; i < 9; ++i) {
		texSamples[i] = texture(hdrColorBuffer, texCoords + kernelCoords[i]).rgb;
	};

	vec3 result = vec3(0.0f);	
	for(int i = 0; i < 9; ++i) {
		result += texSamples[i] * blurKernel[i];
	};

	return result;
};

const float sharpenKernel[9] = float[] (
	 0.0f, -1.0f,  0.0f,
	-1.0f,  5.0f,  -1.0f,
	 0.0f, -1.0f,  0.0f  
);

vec3 sharpen(vec2 texCoords) {
	vec3 texSamples[9];
	for(int i = 0; i < 9; ++i) {
		texSamples[i] = texture(hdrColorBuffer, texCoords + kernelCoords[i]).rgb;
	};

	vec3 result = vec3(0.0f);	
	for(int i = 0; i < 9; ++i) {
		result += texSamples[i] * sharpenKernel[i];
	};

	return result;
};

const float[9] edgeDetectKernel = float[] (
	-1.0f, -1.0f, -1.0f,
	-1.0f,  9.0f, -1.0f,
	-1.0f, -1.0f, -1.0f
);

vec3 edgeDetect(vec2 texCoords) {
	vec3 texSamples[9];
	for(int i = 0; i < 9; ++i) {
		texSamples[i] = texture(hdrColorBuffer, texCoords + kernelCoords[i]).rgb;
	};

	vec3 result = vec3(0.0f);	
	for(int i = 0; i < 9; ++i) {
		result += texSamples[i] * edgeDetectKernel[i];
	};

	return result;
};