# HDR Rendering #

This project is simple a 3D rendering system built with OpenGL for learning purposes.
The Release folder contains a prebuilt x86 binary (windows only). 

![alt text](https://gitlab.com/Uwegamer91/hdr-rendering/-/raw/master/doc/example.jpg?raw=true)

## Controls: ##

- W,A,S,D for movement
- Mouse scroll to zoom in / out
- ESC to end the application
- NUM 8 and 2 to lower or highten the light position
- use the GUI controls to change lighting color etc.
