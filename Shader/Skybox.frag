#version 330 core

in vec3 texCoords;
out vec4 fragmentColor;

uniform samplerCube skybox; 
uniform float gamma;

void main() {
	vec4 result = texture(skybox, texCoords);
	
	result.rgb = pow(result.rgb, vec3(1.0f / gamma));

	fragmentColor = result;
}