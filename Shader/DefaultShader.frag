#version 330 core

#define MAX_POINT_LIGHTS 1

in vec3 fragPos;
in vec3 fragNormal;
in vec4 fragLightPos;
in vec2 fragTexPos;
in vec3 viewPosition;

in vec3 tanFragPos;
in vec3 tanViewPos;
in vec3 tanPointLightPositions[MAX_POINT_LIGHTS];

out vec4 fragmentColor;

struct Material {
	sampler2D diffuse;
	sampler2D specular;
	sampler2D normal;
	sampler2D roughness;
	sampler2D displacement;
    float shininess;
}; 

struct PointLight {
	vec3 position;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;

	float constant;
	float linear;
	float quadratic;

	float farPlane;

	samplerCube shadowCubeMap;
};

struct DirectionalLight {
	vec3 direction;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
};

struct SpotLight {
	vec3 position;
	vec3 direction;
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;

	float constant;
	float linear;
	float quadratic;

	float cutOff;
	float outerCutOff;
};
  
uniform Material material;
uniform samplerCube skybox;
uniform sampler2D shadowMap;

uniform PointLight pointLights[MAX_POINT_LIGHTS];
uniform DirectionalLight globalLightSource;
uniform SpotLight flashLight;
uniform float parallaxHeightScale;
uniform float parallaxMinLayers;
uniform float parallaxMaxLayers;

vec4 calculateDirectionalLight(DirectionalLight light, vec3 normal, vec3 viewDirection, vec2 texPos, float shadow);
vec4 calculatePointLight(PointLight light, vec3 tanLightPos, vec3 normal, vec3 position, vec3 viewDirection, vec2 texPos, float shadow);
vec4 calculateSpotLight(SpotLight light, vec3 normal, vec3 position, vec3 viewDirection, vec2 texPos);
float calculateShadowDirectional(vec4 fragLightSpacePos, vec3 normal);
float calculateShadowOmnidirectional(PointLight light, vec3 fragPos);
vec4 calculateShadowOmnidirectionalDebug(PointLight light, vec3 fragPos);

vec2 parallaxMapping(vec2 texCoords, vec3 viewDir);

const float envReflectIntensity = 0.4f;
const float rougnessBaseLine = 32.0f;

const vec3 shadowCubeMapSampleOffsets[20] = vec3[]
(
   vec3( 1,  1,  1), vec3( 1, -1,  1), vec3(-1, -1,  1), vec3(-1,  1,  1), 
   vec3( 1,  1, -1), vec3( 1, -1, -1), vec3(-1, -1, -1), vec3(-1,  1, -1),
   vec3( 1,  1,  0), vec3( 1, -1,  0), vec3(-1, -1,  0), vec3(-1,  1,  0),
   vec3( 1,  0,  1), vec3(-1,  0,  1), vec3( 1,  0, -1), vec3(-1,  0, -1),
   vec3( 0,  1,  1), vec3( 0, -1,  1), vec3( 0, -1, -1), vec3( 0,  1, -1)
);

void main() {

	vec3 viewDirection = normalize(tanViewPos - tanFragPos);
	vec2 texPos = parallaxMapping(fragTexPos, viewDirection);

	vec3 normal = texture(material.normal, texPos).xyz;
	normal = normalize(normal * 2.0f - 1.0f);
	
	vec3 reflectDirection = reflect(-viewDirection, normal);


//	if(texPos.x < 0.0f || texPos.x > 1.0f || texPos.y < 0.0f || texPos.y > 1.0f)
//		discard;

//	float shadow = calculateShadowDirectional(fragLightPos, normal);
//	vec4 result = calculateDirectionalLight(globalLightSource, normal, viewDirection, texPos, shadow);
	vec4 result = vec4(0.0f, 0.0f, 0.0f, 1.0f);

	for(int i = 0; i < MAX_POINT_LIGHTS; i++) {
		float shadowOmni = calculateShadowOmnidirectional(pointLights[i], fragPos);
		result += calculatePointLight(
			pointLights[i], 
			tanPointLightPositions[i], 
			normal, 
			tanFragPos, 
			viewDirection, 
			texPos, 
			shadowOmni
		);
	}

//	result += calculateSpotLight(flashLight, normal, fragPos, viewDirection, texPos, shadow);
//
//	float roughness = texture(material.roughness, texPos).r * rougnessBaseLine;
//	float spec = pow(max(dot(viewDirection, reflectDirection), 0.0f), roughness);
//	
//	vec4 envReflect = texture(material.specular, texPos).r * texture(skybox, reflectDirection) * spec;
//
//	result += (1.0f - shadow) * envReflect * envReflectIntensity;

//	if(useGammaCorrection == 1) {
//		result.rgb = pow(result.rgb, vec3(1.0f / gamma));
//	}
		
	fragmentColor = vec4(result.rgb, 1.0f);

//	fragmentColor = calculateShadowOmnidirectionalDebug(pointLights[0], fragPos);
}

vec4 calculateDirectionalLight(DirectionalLight light, vec3 normal, vec3 viewDirection, vec2 texPos, float shadow) {
	vec3 lightDirection = normalize(-light.direction);
//	vec3 reflectDirection = reflect(-lightDirection, normal);
	vec3 halfWay = normalize(lightDirection + viewDirection);

	float diff = max(dot(normal, lightDirection), 0.0f);
	float roughness = texture(material.roughness, texPos).r * rougnessBaseLine;
	float spec = pow(max(dot(viewDirection, halfWay), 0.0f), roughness * 4);

	vec4 ambient  = vec4(light.ambient, 1.0f) * texture(material.diffuse, texPos);
	vec4 diffuse  = vec4(light.diffuse, 1.0f) * diff * texture(material.diffuse, texPos);
	vec4 specular = vec4(light.specular, 1.0f) * spec * texture(material.specular, texPos).r;

	return ambient + (1.0f - shadow) * (diffuse + specular);
};

vec4 calculatePointLight(PointLight light, vec3 tanLightPos, vec3 normal, vec3 position, vec3 viewDirection, vec2 texPos, float shadow) {
	vec3 lightDirection = normalize(tanLightPos - position);
//	vec3 reflectDirection = reflect(-lightDirection, normal);

	vec3 halfWay = normalize(lightDirection + viewDirection);

	float diff = max(dot(normal, lightDirection), 0.0f);
	float roughness = texture(material.roughness, texPos).r * rougnessBaseLine;
	float spec = pow(max(dot(viewDirection, halfWay), 0.0f), roughness * 4);

	vec4 color = texture(material.diffuse, texPos);

	vec4 ambient  = vec4(light.ambient, 1.0f) * color;
	vec4 diffuse  = vec4(light.diffuse, 1.0f) * diff * color;
	vec4 specular = vec4(light.specular, 1.0f) * spec * texture(material.specular, texPos).r;

	float pointLightDistance = length(tanLightPos - position);
	float pointLightAttenuation = 1.0f / 
		( light.constant + 
		  light.linear * pointLightDistance + 
		  light.quadratic * (pointLightDistance * pointLightDistance) );

	ambient *= pointLightAttenuation;
	diffuse *= pointLightAttenuation;
	specular *= pointLightAttenuation;

	return ambient + (1.0f - shadow) * (diffuse + specular);
};

vec4 calculateSpotLight(SpotLight light, vec3 normal, vec3 position, vec3 viewDirection, vec2 texPos) {
	vec3 lightDirection = normalize(light.position - position);
//	vec3 reflectDirection = reflect(-lightDirection, normal);
	vec3 halfWay = normalize(lightDirection + viewDirection);

	float diff = max(dot(normal, lightDirection), 0.0f);

	float roughness = texture(material.roughness, texPos).r * rougnessBaseLine;
	float spec = pow(max(dot(viewDirection, halfWay), 0.0f), roughness * 4);

	vec4 ambient = vec4(light.ambient, 1.0f) * texture(material.diffuse, texPos);
	vec4 diffuse = vec4(light.diffuse, 1.0f) * diff * texture(material.diffuse, texPos);
	vec4 specular = vec4(light.specular, 1.0f) * spec * texture(material.specular, texPos).r;

	float dist			= length(light.position - position);
	float attenuation	= 1.0f / ( light.constant + light.linear * dist + light.quadratic * (dist * dist) );

	ambient  *= attenuation;
	diffuse  *= attenuation;
	specular *= attenuation;

	float theta = dot(normalize(-light.direction), lightDirection);
	float epsilon = light.cutOff - light.outerCutOff;

	float intensity	= clamp((theta - light.outerCutOff) / epsilon, 0.0f, 1.0f);

	ambient  *= intensity;
	diffuse  *= intensity;
	specular *= intensity;

	return ambient + diffuse + specular;
};

float calculateShadowDirectional(vec4 fragLightSpacePos, vec3 normal) {

	float bias = max( 0.01f * dot(globalLightSource.direction, normal), 0.001f);
	vec3 lightSpaceNDC = (fragLightSpacePos.xyz / fragLightSpacePos.w) * 0.5f + 0.5f;

	float shadow = 0.0f;

	if(lightSpaceNDC.z <= 1.0f) {
		vec2 texelSize = 1.0f / textureSize(shadowMap, 0);

		float currentDepth = lightSpaceNDC.z;

		for(int x = -1; x <= 1; ++x) {

			for(int y = -1; y <= 1; ++y) {

				float pcfDepth = texture(shadowMap, lightSpaceNDC.xy + vec2(x,y) * texelSize).r;
				shadow += (currentDepth - bias) > pcfDepth ? 1.0f : 0.0f;
			}

		}
		
		shadow /= 9.0f;
	}

	return shadow;
};

float calculateShadowOmnidirectional(PointLight light, vec3 fragPos) {

	float shadow = 0.0f;
	float bias = 0.05f;
	float viewDistance = length(fragPos - viewPosition);
	float offsetRadius = (1.0f + viewDistance / light.farPlane) / 25.0f;
	int samples = 20;

	vec3 lightToFrag = fragPos - light.position;

	float currentDepth = length(lightToFrag);

	if(currentDepth < light.farPlane) {

		for(int i = 0; i < samples; ++i) {
			float closestDepth = texture(light.shadowCubeMap, lightToFrag + shadowCubeMapSampleOffsets[i] * offsetRadius).r;
			closestDepth *= light.farPlane;

			if(currentDepth - bias > closestDepth)
				shadow += 1.0f;
		}

		shadow /= float(samples);

	}

	return shadow;
};


vec4 calculateShadowOmnidirectionalDebug(PointLight light, vec3 fragPos) {
	vec3 lightToFrag = fragPos - light.position;
	
	float closestDepth = texture(light.shadowCubeMap, lightToFrag).r;
//	closestDepth *= light.farPlane;

	float currentDepth = length(lightToFrag);

	return vec4(vec3(closestDepth / light.farPlane), 1.0f);
};

vec2 parallaxMapping(vec2 texCoords, vec3 viewDir) {
	float parallaxLayers = mix(parallaxMaxLayers, parallaxMinLayers, abs(dot(vec3(0.0f, 0.0f, 1.0f), viewDir)));
	float layerDepth = 1.0f / parallaxLayers;
	float currentLayerDepth = 0.0f;

	vec2 currentTexCoords = texCoords;
	float currentDepth = 1.0f - texture(material.displacement, currentTexCoords).r;

	vec2 p = viewDir.xy * parallaxHeightScale;
	vec2 deltaTexCoords = p / parallaxLayers;

	while(currentLayerDepth < currentDepth) {
		currentTexCoords -= deltaTexCoords;
		currentDepth = 1.0f - texture(material.displacement, currentTexCoords).r;
		currentLayerDepth += layerDepth;
	};

	vec2 prevTexCoords = currentTexCoords + deltaTexCoords;
	
	float prevDepth = 1.0f - texture(material.displacement, prevTexCoords).r;

	float depthBeforeCollision = prevDepth - currentLayerDepth + layerDepth;
	float depthAfterCollision = currentDepth - currentLayerDepth;

	float weight = depthAfterCollision / (depthAfterCollision - depthBeforeCollision);

	vec2 mappedTexCoords = prevTexCoords * weight + currentTexCoords * (1.0f - weight);
	return mappedTexCoords;
};