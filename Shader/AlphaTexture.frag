#version 330 core

in vec3 fragPos;
in vec3 fragNormal;
in vec2 fragTexPos;

out vec4 fragmentColor;

uniform sampler2D diffuseMap;
uniform int postProcessingMode;
uniform float gamma;

#define PP_INVERSION	0x0000
#define PP_GREYSCALE	0x0001
#define PP_BOXBLUR		0x0002
#define PP_SHARPEN		0x0003
#define PP_EDGE_DETECT	0x0004

vec4 invert(vec4 color);
vec4 greyScale(vec4 color);
vec4 boxBlur(vec2 texCoords);
vec4 sharpen(vec2 texCoords);
vec4 edgeDetect(vec2 texCoords);

const float kernelOffset = 1.0f / 300.0f;
vec2 kernelCoords[9];

void main() {
	kernelCoords[0] = vec2(-kernelOffset, kernelOffset);
	kernelCoords[1] = vec2(0.0f, kernelOffset);
	kernelCoords[2] = vec2(kernelOffset, kernelOffset);
	kernelCoords[3] = vec2(-kernelOffset, 0.0f);
	kernelCoords[4] = vec2(0.0f, 0.0f);
	kernelCoords[5] = vec2(kernelOffset, 0.0f);
	kernelCoords[6] = vec2(-kernelOffset, -kernelOffset);
	kernelCoords[7] = vec2(0.0f, -kernelOffset);
	kernelCoords[8] = vec2(kernelOffset, -kernelOffset);

    vec4 result = vec4(1.0f);
	vec4 texColor =  texture(diffuseMap, fragTexPos);

	switch(postProcessingMode) {
		case PP_INVERSION:
			result = invert(texColor);
			break;
		case PP_GREYSCALE:
			result = greyScale(texColor);
			break;
		case PP_BOXBLUR:
			result = boxBlur(fragTexPos);
			break;
		case PP_SHARPEN:
			result = sharpen(fragTexPos);
			break;
		case PP_EDGE_DETECT:
			result = edgeDetect(fragTexPos);
			break;
	};

	result.rgb = pow(result.rgb, vec3(1.0f / gamma));

	fragmentColor = result;
}

vec4 invert(vec4 color) {
	return vec4(vec3(1.0f) - color.rgb, color.a);
};

vec4 greyScale(vec4 color) {
	float average = 0.2126f * color.r + 0.7152f * color.g + 0.0722f * color.b;
	return vec4(average, average, average, color.a);
};

vec4 boxBlur(vec2 texCoords) {
	float kernel[9];
	for(int i = 0; i < 9; ++i) {
		kernel[i] = 1.0f / 9.0f;
	};

	vec4 texSamples[9];
	for(int i = 0; i < 9; ++i) {
		texSamples[i] = texture(diffuseMap, texCoords + kernelCoords[i]);
	};

	vec4 result = vec4(0.0f);	
	for(int i = 0; i < 9; ++i) {
		result += texSamples[i] * kernel[i];
	};

	return result;
};

vec4 sharpen(vec2 texCoords) {
	float kernel[9];
	kernel[0] = 0.0f;
	kernel[1] = -1.0f;
	kernel[2] = 0.0f,
	kernel[3] = -1.0f;
	kernel[4] = 5.0f;
	kernel[5] -1.0f;
	kernel[6] = 0.0f;
	kernel[7] = -1.0f;
	kernel[8] = 0.0f;
	
	vec4 texSamples[9];
	for(int i = 0; i < 9; ++i) {
		texSamples[i] = texture(diffuseMap, texCoords + kernelCoords[i]);
	};

	vec4 result = vec4(0.0f);	
	for(int i = 0; i < 9; ++i) {
		result += texSamples[i] * kernel[i];
	};

	return result;
};

vec4 edgeDetect(vec2 texCoords) {
	float kernel[9];

	for(int i = 0; i < 9; ++i) {
		kernel[i] = -1.0f;
	};
	kernel[4] = 9.0f;

	vec4 texSamples[9];
	for(int i = 0; i < 9; ++i) {
		texSamples[i] = texture(diffuseMap, texCoords + kernelCoords[i]);
	};

	vec4 result = vec4(0.0f);	
	for(int i = 0; i < 9; ++i) {
		result += texSamples[i] * kernel[i];
	};

	return result;
};