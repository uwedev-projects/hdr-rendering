#include "GLErrorHandler.h"
#include <stdexcept>

const std::unordered_map<GLenum, std::string, EnumHasher<GLenum>> GLErrorHandler::errorMap = {
	{ GL_NO_ERROR, "No OpenGL error occured." },
	{ GL_INVALID_ENUM, "Invalid enum." },
	{ GL_INVALID_VALUE, "Invalid value." },
	{ GL_INVALID_OPERATION, "Invalid operation." },
	{ GL_INVALID_FRAMEBUFFER_OPERATION, "Invalid framebuffer operation." },
	{ GL_OUT_OF_MEMORY, "OpenGL ran out of memory." }
};

GLErrorHandler::GLErrorHandler()
{
}


GLErrorHandler::~GLErrorHandler()
{
}

std::string GLErrorHandler::getLastError() const {
	GLenum error{ glGetError() };
	std::string errMsg{};

	try {
		errMsg = GLErrorHandler::errorMap.at(error);
	}
	catch (std::out_of_range& e) {}

	return errMsg;
};

void GLErrorHandler::throwOnError() const {
	GLenum error{ glGetError() };
	if (error != GL_NO_ERROR) {
		std::string errMsg{ "Aborted due to OpenGL error: " };
		errMsg += GLErrorHandler::errorMap.at(error);
		throw std::exception{ errMsg.c_str() };
	}
};