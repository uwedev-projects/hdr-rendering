#include "Constants.h"

#include <glm/gtc/constants.hpp>

namespace Constants {

	const char* glslVersion{ "#version 330 core" };

	std::vector<PointLight> pointLights{
		PointLight {
			glm::vec3{ 1.0f, 6.0f, -4.0f },
			glm::vec3{ 0.2f, 0.2f, 0.2f },
			glm::vec3{ 100.0f, 100.0f, 100.0f },
			glm::vec3{ 200.0f, 200.0f, 200.0f },
			.5f, 0.08f, 0.02f, 7
		},
		//PointLight{
		//	glm::vec3{ -2.0f, -7.0f, -1.0f },
		//	glm::vec3{ 0.2f, 0.2f, 0.2f },
		//	glm::vec3{ 100.0f, 100.0f, 100.0f  },
		//	glm::vec3{ 1.0f, 1.0f, 1.0f },
		//	.5f, 0.05f, 0.01f, 8
		//},
		//PointLight{
		//	glm::vec3{ -4.0f, -7.0f, -1.0f },
		//	glm::vec3{ 0.2f, 0.2f, 0.2f },
		//	glm::vec3{ 100.0f, 100.0f, 100.0f },
		//	glm::vec3{ 1.0f, 1.0f, 1.0f },
		//	.5f, 0.05f, 0.01f, 9
		//},
		//PointLight{
		//	glm::vec3{ 0.0f, -7.0f, -1.0f },
		//	glm::vec3{ 0.2f, 0.2f, 0.2f },
		//	glm::vec3{ 100.0f, 100.0f, 100.0f },
		//	glm::vec3{ 1.0f, 1.0f, 1.0f },
		//	.5f, 0.05f, 0.01f, 10
		//}
	};

	glm::vec3 rotationCenters[]{
		{ 4.0f, 2.0f, 0.0f },
		{ -2.0f, 5.0f, 5.0f },
		{ -5.0f, 3.0f, -6.0f },
		{  0.0f, 7.0f, -3.0f }
	};

	float radii[]{ 8.0f, 7.0f, 8.0f, 10.0f };

	extern glm::vec3 rotationAxis[]{
		{ 0.0f, 0.0f, 0.0f },
		{ 1.0f, 1.0f, 0.0f },
		{ 0.0f, 1.0f, 1.0f },
		{ 1.0f, 0.0f, 1.0f },
	};
	extern float rotations[]{ 0.0f, glm::quarter_pi<float>(), glm::half_pi<float>(), glm::pi<float>() };

	float floorVertices[]{
		// pos					// norm				// tex			// tangent
		-1.0f, 0.0f,  1.0f,		0.0f, 1.0f, 0.0f,	0.0f, 0.0f,		0.0f, 0.0f, -1.0f,
		 1.0f, 0.0f,  1.0f,		0.0f, 1.0f, 0.0f,	40.0f, 0.0f,	0.0f, 0.0f, -1.0f,
		 1.0f, 0.0f, -1.0f,		0.0f, 1.0f, 0.0f,	40.0f, 40.0f,	0.0f, 0.0f, -1.0f,
		 1.0f, 0.0f, -1.0f,		0.0f, 1.0f, 0.0f,	40.0f, 40.0f,	0.0f, 0.0f, -1.0f,
		-1.0f, 0.0f, -1.0f,		0.0f, 1.0f, 0.0f,	0.0f, 40.0f,	0.0f, 0.0f, -1.0f,
		-1.0f, 0.0f,  1.0f,		0.0f, 1.0f, 0.0f,	0.0f, 0.0f,		0.0f, 0.0f, -1.0f
	};

	glm::vec3 floorPosition{ 0.0f, -10.0f, 0.0f };

	float fireVertices[]{
		-1.0f,  1.0f, 1.0f,		0.0f, 0.0f, 1.0f,	0.0f, 1.0f,		0.0f, 1.0f, 0.0f,
		 1.0f, -1.0f, 1.0f,		0.0f, 0.0f, 1.0f,	1.0f, 0.0f,		0.0f, 1.0f, 0.0f,
		 1.0f,  1.0f, 1.0f,		0.0f, 0.0f, 1.0f,	1.0f, 1.0f,		0.0f, 1.0f, 0.0f,
		 1.0f, -1.0f, 1.0f,		0.0f, 0.0f, 1.0f,	1.0f, 0.0f,		0.0f, 1.0f, 0.0f,
		-1.0f,  1.0f, 1.0f,		0.0f, 0.0f, 1.0f,	0.0f, 1.0f,		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 1.0f,		0.0f, 0.0f, 1.0f,	0.0f, 0.0f,		0.0f, 1.0f, 0.0f,
	};
}