#include <glm/gtc/matrix_transform.hpp>

#include "Camera3D.h"
#include "Constants.h"

Camera3D::Camera3D()
	:position{ 0.0f }, front{ 0.0f }, worldUp{ 0.0f }, fov{ 1.0f }, right{ 0.0f }, up{ 0.0f }, pitch{}, yaw{},
	speed{ 1.0f }, screenWidth{Constants::screenWidth}, screenHeight{Constants::screenHeight}
{
}

Camera3D::Camera3D(const glm::vec3& cameraPosition, const glm::vec3& front, const glm::vec3& worldUp, float fov, float pitch, float yaw, float sensitivity)
	: position{ cameraPosition }, front{ front }, worldUp{ worldUp }, fov{ fov }, right{}, up{},
	pitch{ pitch }, yaw{ yaw }, speed{ 1.0f }, sensitivity{ sensitivity }, screenWidth{Constants::screenWidth}, screenHeight{Constants::screenHeight}
{
	updateVectors();
}

Camera3D::~Camera3D()
{
}

const glm::vec3& Camera3D::getPosition() const noexcept {
	return position;
};

const glm::vec3& Camera3D::getFront() const noexcept {
	return front;
};

const glm::vec3& Camera3D::getRight() const noexcept {
	return right;
};

const glm::vec3& Camera3D::getUp() const noexcept {
	return up;
};

float Camera3D::getSpeed() const noexcept {
	return speed;
};

float Camera3D::getFov() const noexcept {
	return fov;
};

glm::mat4 Camera3D::getViewMatrix() const {
	return glm::lookAt(position, position + front, up);
};

glm::mat4 Camera3D::getProjectionMatrix() const {
	return glm::perspective(glm::radians(fov), (float) (screenWidth / screenHeight), 0.1f, 100.0f);
};

void Camera3D::setPosition(const glm::vec3 cameraPosition) {
	position = cameraPosition;
};

void Camera3D::setFront(const glm::vec3 front) {
	this->front = front;
	updateVectors();
};

void Camera3D::setTarget(const glm::vec3 target) {
	front = glm::normalize(target - position);
	updateVectors();
};

void Camera3D::setSpeed(float speed) noexcept {
	this->speed = speed;
};

void Camera3D::setSensitivity(float sensitivity) noexcept {
	this->sensitivity = sensitivity;
};

void Camera3D::setScreenDimensions(unsigned int width, unsigned int height) noexcept {
	screenWidth = width;
	screenHeight = height;
};

void Camera3D::moveForward(float deltaT) {
	position += speed * deltaT * front;
};

void Camera3D::moveBackward(float deltaT) {
	position -= speed * deltaT * front;
};

void Camera3D::moveRight(float deltaT) {
	position += speed * deltaT * right;
};

void Camera3D::moveLeft(float deltaT) {
	position -= speed * deltaT * right;
};

void Camera3D::processMouseMovement(float offsetX, float offsetY) {
	yaw += offsetX * sensitivity;
	pitch += offsetY * sensitivity;

	if (pitch > 89.0f)
		pitch = 89.0f;
	if (pitch < -89.0f)
		pitch = -89.0f;
	
	updateVectors();
};

void Camera3D::processMouseScroll(float offset) {
	fov -= offset;
	
	if (fov > 45.0f)
		fov = 45.0f;

	if (fov < 1.0f)
		fov = 1.0f;
};

void Camera3D::updateVectors() {
	float frontX{ cos(glm::radians(yaw)) * cos(glm::radians(pitch)) };
	float frontY{ sin(glm::radians(pitch)) };
	float frontZ{ sin(glm::radians(yaw)) * cos(glm::radians(pitch)) };

	front = glm::normalize(glm::vec3{ frontX, frontY, frontZ });
	right = glm::normalize(glm::cross(front, worldUp));
	up	  = glm::normalize(glm::cross(right, front));
};