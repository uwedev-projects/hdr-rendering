#include "PointLight.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glad/glad.h>

#include "Constants.h"
#include "GLErrorHandler.h"

#include <iostream>

PointLight::PointLight(const glm::vec3& position, const glm::vec3& ambient, const glm::vec3& diffuse, const glm::vec3& specular)
	: position{ position }, ambient{ ambient }, diffuse{ diffuse }, specular{ specular },
	constant{ DefaultConstant }, linear{ DefaultLinear }, quadratic{ DefaultQuadratic },
	depthMapFBO{}, depthMap{}, texUnit{}
{
}

PointLight::PointLight(const glm::vec3& position, const glm::vec3& ambient, const glm::vec3& diffuse, const glm::vec3& specular,
	float constant, float linear, float quadratic, unsigned int texUnit)
	: position{ position }, ambient{ ambient }, diffuse{ diffuse }, specular{ specular },
	constant{ constant }, linear{ linear }, quadratic{ quadratic },
	depthMapFBO{}, depthMap{}, texUnit{texUnit}
{
}

void PointLight::initialize() {

	glGenFramebuffers(1, &depthMapFBO);

	glGenTextures(1, &depthMap);
	glBindTexture(GL_TEXTURE_CUBE_MAP, depthMap);

	for (unsigned int face = 0; face < 6; ++face) {
		glTexImage2D(
			GL_TEXTURE_CUBE_MAP_POSITIVE_X + face, 
			0, 
			GL_DEPTH_COMPONENT, 
			Constants::depthMapWidth, 
			Constants::depthMapHeight, 
			0, 
			GL_DEPTH_COMPONENT, 
			GL_FLOAT,
			nullptr
		);

		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	}

	glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
	glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, depthMap, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		std::cout << "ERROR: Failed to complete frame buffer for shadow cube map." << std::endl;

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
};

std::vector<glm::mat4> PointLight::getLightSpaceMatrices(unsigned int screenWidth, unsigned int screenHeight) const {
	std::vector<glm::mat4> matrices{};

	const float aspect{ (float) screenWidth / (float) screenHeight };

	glm::mat4 proj{ glm::perspective(glm::radians(90.0f), aspect, DefaultNear, DefaultFar) };

	matrices.push_back(proj * glm::lookAt(position, position + glm::vec3{  1.0f,  0.0f,  0.0f }, glm::vec3{  0.0f, -1.0f,  0.0f }));
	matrices.push_back(proj * glm::lookAt(position, position + glm::vec3{ -1.0f,  0.0f,  0.0f }, glm::vec3{  0.0f, -1.0f,  0.0f }));
	matrices.push_back(proj * glm::lookAt(position, position + glm::vec3{  0.0f,  1.0f,  0.0f }, glm::vec3{  0.0f,  0.0f,  1.0f }));
	matrices.push_back(proj * glm::lookAt(position, position + glm::vec3{  0.0f, -1.0f,  0.0f }, glm::vec3{  0.0f,  0.0f,  -1.0f }));
	matrices.push_back(proj * glm::lookAt(position, position + glm::vec3{  0.0f,  0.0f,  1.0f }, glm::vec3{  0.0f,  -1.0f,  0.0f }));
	matrices.push_back(proj * glm::lookAt(position, position + glm::vec3{  0.0f,  0.0f, -1.0f }, glm::vec3{  0.0f,  -1.0f,  0.0f }));

	return matrices;
};

unsigned int PointLight::getCubeMapFramebuffer() const noexcept {
	return depthMapFBO;
};

unsigned int PointLight::getDepthMapTexture() const noexcept {
	return depthMap;
};

float PointLight::getFarPlane() const noexcept {
	return DefaultFar;
};

const glm::vec3& PointLight::getPosition() const noexcept {
	return position;
};

const glm::vec3& PointLight::getAmbient() const noexcept {
	return ambient;
};

const glm::vec3& PointLight::getDiffuse() const noexcept {
	return diffuse;
};

const glm::vec3& PointLight::getSpecular() const noexcept {
	return specular;
};

float PointLight::getConstant() const noexcept {
	return constant;
};

float PointLight::getLinear() const noexcept {
	return linear;
};

float PointLight::getQuadratic() const noexcept {
	return quadratic;
};

unsigned int PointLight::getTextureUnit() const noexcept {
	return texUnit;
};

void PointLight::setPosition(const glm::vec3& pos) {
	position = pos;
};

void PointLight::setDiffuse(const glm::vec3& diff) {
	diffuse = diff;
};

void PointLight::setSpecular(const glm::vec3& spec) {
	specular = spec;
};