#define IMGUI_IMPL_OPENGL_LOADER_GLAD
#undef IMGUI_IMPL_OPENGL_LOADER_GL3W

#include <imgui/imgui.h>
#include <imgui/imgui_impl_glfw.h>
#include <imgui/imgui_impl_opengl3.h>

#include <glad/glad.h> 
#include <GLFW/glfw3.h>
#include <stb_image.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp> 
#include <glm/gtc/type_ptr.hpp>
#include <glm/mat4x4.hpp>

#include <iostream>
#include <thread>
#include <chrono>
#include <utility>

#include "Shader.h"
#include "Camera3D.h"
#include "LightSource.h"
#include "DirectionalLight.h"
#include "Constants.h"
#include "GLErrorHandler.h"

void drawScene(bool useOffScreenBuffer = false);
void drawCubes(Shader& shader);
void drawRoom();
void drawHDRTexture();
void drawUserInterface(float deltaT);
void renderShadowMap(void);
void renderShadowCubeMaps(void);
void renderImGui(void);
void updateLights(float deltaT);
void setVertexAttributePointers(void);
void processInput(GLFWwindow* window, Camera3D& camera, float deltaT);

std::pair<unsigned int, unsigned int> createHDRFrameBuffer(void);
std::pair<unsigned int, unsigned int> createFrameBufferMultiSampled(void);
std::pair<unsigned int, unsigned int> createHDRFrameBufferMultisampled(void);

unsigned int loadTexture(const std::string& textureFile);
unsigned int loadCubeMap(const std::vector<std::string>& textureFiles);
void frameBufferSizeCallback(GLFWwindow* window, int width, int height);
void mouseMoveCallback(GLFWwindow* window, double xPos, double yPos);
void mouseScrollCallback(GLFWwindow* window, double offsetX, double offsetY);
void characterCallback(GLFWwindow* window, unsigned int codepoint);
void errorCallback(int err, const char* msg);

#ifdef _DEBUG_
const char* appBasePath = "..\\HDR\\";
#else
const char* appBasePath = "";
#endif

float screenVertices[]{
	// position				// tex coords
	-1.0f,  1.0f, 0.0f,		0.0f, 1.0f,
	-1.0f, -1.0f, 0.0f,		0.0f, 0.0f,
	 1.0f,  1.0f, 0.0f,		1.0f, 1.0f,
	 1.0f, -1.0f, 0.0f,		1.0f, 0.0f,
};

Constants::PostProcessing postProcessingMode{ Constants::PostProcessing::None };

float vertexData[]{
	// positions			// normals 				// texture coords	// tangent 
	-1.0f, -1.0f, -1.0f,	0.0f, 0.0f, -1.0f,		0.0f, 0.0f,			-1.0f, 0.0f, 0.0f,
	-1.0f,  1.0f, -1.0f,	0.0f, 0.0f, -1.0f,		0.0f, 1.0f,			-1.0f, 0.0f, 0.0f,
	 1.0f, -1.0f, -1.0f,	0.0f, 0.0f, -1.0f,		1.0f, 0.0f,			-1.0f, 0.0f, 0.0f,
	 1.0f, -1.0f, -1.0f,	0.0f, 0.0f, -1.0f,		1.0f, 0.0f,			-1.0f, 0.0f, 0.0f,
	-1.0f,  1.0f, -1.0f,	0.0f, 0.0f, -1.0f,		0.0f, 1.0f,			-1.0f, 0.0f, 0.0f,
	 1.0f,  1.0f, -1.0f,	0.0f, 0.0f, -1.0f,		1.0f, 1.0f,			-1.0f, 0.0f, 0.0f,

	-1.0f, -1.0f,  1.0f,	-1.0f, 0.0f, 0.0f,		0.0f, 0.0f,			 0.0f, 0.0f, -1.0f,
	-1.0f,  1.0f,  1.0f,	-1.0f, 0.0f, 0.0f,		0.0f, 1.0f,			 0.0f, 0.0f, -1.0f,
	-1.0f, -1.0f, -1.0f,	-1.0f, 0.0f, 0.0f,		1.0f, 0.0f,			 0.0f, 0.0f, -1.0f,
	-1.0f,  1.0f, -1.0f,	-1.0f, 0.0f, 0.0f,		1.0f, 1.0f,			 0.0f, 0.0f, -1.0f,
	-1.0f, -1.0f, -1.0f,	-1.0f, 0.0f, 0.0f,		1.0f, 0.0f,			 0.0f, 0.0f, -1.0f,
	-1.0f,  1.0f,  1.0f,	-1.0f, 0.0f, 0.0f,		0.0f, 1.0f,			 0.0f, 0.0f, -1.0f,

	 1.0f, -1.0f, -1.0f,	1.0f, 0.0f, 0.0f,		1.0f, 0.0f,			 0.0f, 0.0f, -1.0f,
	 1.0f,  1.0f, -1.0f,	1.0f, 0.0f, 0.0f,		1.0f, 1.0f,			 0.0f, 0.0f, -1.0f,
	 1.0f,  1.0f,  1.0f,	1.0f, 0.0f, 0.0f,		0.0f, 1.0f,			 0.0f, 0.0f, -1.0f,
	 1.0f, -1.0f,  1.0f,	1.0f, 0.0f, 0.0f,		0.0f, 0.0f,			 0.0f, 0.0f, -1.0f,
	 1.0f, -1.0f, -1.0f,	1.0f, 0.0f, 0.0f,		1.0f, 0.0f,			 0.0f, 0.0f, -1.0f,
	 1.0f,  1.0f,  1.0f,	1.0f, 0.0f, 0.0f,		0.0f, 1.0f,			 0.0f, 0.0f, -1.0f,
		
	-1.0f, -1.0f,  1.0f,	0.0f, 0.0f, 1.0f,		0.0f, 0.0f,			 1.0f, 0.0f, 0.0f,
	 1.0f, -1.0f,  1.0f,	0.0f, 0.0f, 1.0f,		1.0f, 0.0f,			 1.0f, 0.0f, 0.0f,
	 1.0f,  1.0f,  1.0f,	0.0f, 0.0f, 1.0f,		1.0f, 1.0f,			 1.0f, 0.0f, 0.0f,
	-1.0f,  1.0f,  1.0f,	0.0f, 0.0f, 1.0f,		0.0f, 1.0f,			 1.0f, 0.0f, 0.0f,
	-1.0f, -1.0f,  1.0f,	0.0f, 0.0f, 1.0f,		0.0f, 0.0f,			 1.0f, 0.0f, 0.0f,
	 1.0f,  1.0f,  1.0f,	0.0f, 0.0f, 1.0f,		1.0f, 1.0f,			 1.0f, 0.0f, 0.0f,

	-1.0f, -1.0f,  1.0f,	0.0f, -1.0f, 0.0f,		0.0f, 0.0f,			 0.0f, 0.0f, 1.0f,
	-1.0f, -1.0f, -1.0f,	0.0f, -1.0f, 0.0f,		0.0f, 1.0f,			 0.0f, 0.0f, 1.0f,
	 1.0f, -1.0f,  1.0f,	0.0f, -1.0f, 0.0f,		1.0f, 0.0f,			 0.0f, 0.0f, 1.0f,
	 1.0f, -1.0f, -1.0f,	0.0f, -1.0f, 0.0f,		1.0f, 1.0f,			 0.0f, 0.0f, 1.0f,
	 1.0f, -1.0f,  1.0f,	0.0f, -1.0f, 0.0f,		1.0f, 0.0f,			 0.0f, 0.0f, 1.0f,
	-1.0f, -1.0f, -1.0f,	0.0f, -1.0f, 0.0f,		0.0f, 1.0f,			 0.0f, 0.0f, 1.0f,

	-1.0f, 1.0f,  1.0f,		0.0f, 1.0f, 0.0f,		0.0f, 0.0f,			1.0f, 0.0f, 0.0f,
	 1.0f, 1.0f,  1.0f,		0.0f, 1.0f, 0.0f,		1.0f, 0.0f,			1.0f, 0.0f, 0.0f,
	 1.0f, 1.0f, -1.0f,		0.0f, 1.0f, 0.0f,		1.0f, 1.0f,			1.0f, 0.0f, 0.0f,
	-1.0f, 1.0f, -1.0f,		0.0f, 1.0f, 0.0f,		0.0f, 1.0f,			1.0f, 0.0f, 0.0f,
	-1.0f, 1.0f,  1.0f,		0.0f, 1.0f, 0.0f,		0.0f, 0.0f,			1.0f, 0.0f, 0.0f,
	 1.0f, 1.0f, -1.0f,		0.0f, 1.0f, 0.0f,		1.0f, 1.0f,			1.0f, 0.0f, 0.0f,
};

float skyboxVertices[]{
	// positions
	-1.0f, -1.0f, -1.0f,
	-1.0f,  1.0f, -1.0f,
	 1.0f, -1.0f, -1.0f,
	 1.0f, -1.0f, -1.0f,
	-1.0f,  1.0f, -1.0f,
	 1.0f,  1.0f, -1.0f,

	-1.0f, -1.0f,  1.0f,
	-1.0f,  1.0f,  1.0f,
	-1.0f, -1.0f, -1.0f,
	-1.0f,  1.0f, -1.0f,
	-1.0f, -1.0f, -1.0f,
	-1.0f,  1.0f,  1.0f,

	 1.0f, -1.0f, -1.0f,
	 1.0f,  1.0f, -1.0f,
	 1.0f,  1.0f,  1.0f,
	 1.0f, -1.0f,  1.0f,
	 1.0f, -1.0f, -1.0f,
	 1.0f,  1.0f,  1.0f,

	-1.0f, -1.0f,  1.0f,
	 1.0f, -1.0f,  1.0f,
	 1.0f,  1.0f,  1.0f,
	-1.0f,  1.0f,  1.0f,
	-1.0f, -1.0f,  1.0f,
	 1.0f,  1.0f,  1.0f,

	-1.0f, -1.0f,  1.0f,
	-1.0f, -1.0f, -1.0f,
	 1.0f, -1.0f,  1.0f,
	 1.0f, -1.0f, -1.0f,
	 1.0f, -1.0f,  1.0f,
	-1.0f, -1.0f, -1.0f,

	-1.0f, 1.0f, -1.0f,
	-1.0f, 1.0f,  1.0f,
	 1.0f, 1.0f, -1.0f,
	-1.0f, 1.0f,  1.0f,
	 1.0f, 1.0f,  1.0f,
	 1.0f, 1.0f, -1.0f,
};

glm::vec3 cubePositions[]{
	{  0.0f,  2.0f, -1.0f },
	{  1.5f, -5.0f, -3.0f },
	{ -3.0f,  2.0f, -4.0f },
	{  5.0f,  3.0f, 2.0f },
	{  3.0f, -1.0f, 8.0f },
	{ -1.0f,  2.0f, -12.5f },
	{  0.0f, -5.0f, -9.0f },
	{  2.5f, -1.0f, -3.0f },
	{ -2.0f,  3.0f, 15.0f }
};

unsigned int screenWidth{ Constants::screenWidth }, screenHeight{ Constants::screenHeight };
int configPanelWidth{ Constants::configPanelWidth }, configPanelHeight{ Constants::configPanelHeight };

unsigned int vertexBuffer{}, elementBuffer{}, cubeVAO{}, lightSourceVAO{};
unsigned int screenVBO, screenVAO;
unsigned int floorVertexBuffer{}, floorVertexArray{};
unsigned int fireVertexBuffer{}, fireVertexArray{};
unsigned int skyboxVertexBuffer{}, skyboxVertexArray{};

unsigned int cubeDiffuseMap{}, cubeSpecularMap{}, cubeNormalMap{}, cubeRoughnessMap{}, cubeDisplacementMap{};
unsigned int floorDiffuseMap{}, floorSpecularMap{}, floorNormalMap{}, floorRoughnessMap{}, floorDisplacementMap{}, fireDiffuse{};
unsigned int skybox{};

unsigned int frameBuffer{}, frameBufferTexture{};
unsigned int multiSampledFBO{}, multiSampledTexture{};
unsigned int hdrFBO{}, hdrTexture{};
unsigned int depthMapFBO{}, depthMap{};

float frameCount{};
float secondsElapsed{};

float exposure{ 1.0f };
float gamma{ 2.2f };
float parallaxHeightScale{ 0.1f };
int parallaxMinLayers{ 8 };
int parallaxMaxLayers{ 32 };

Material material{ 0, 1, 2, 3, 4, 1.0f };

Shader defaultShader, lightSourceShader, alphaShader, skyboxShader, simpleDepth, omnidirectionalShadow, hdrShader;

Camera3D camera{
	glm::vec3{ 2.0f, 2.0f,   15.0f },
	glm::vec3{ 0.0f, 0.0f,  -1.0f },
	glm::vec3{ 0.0f, 1.0f,   0.0f },
	45.0f
};
float cameraSpeed{ 5.5f };

DirectionalLight directionalLight{
	glm::vec3{ 0.5f, -1.0f, -0.5f },
	glm::vec3{ 0.005f, 0.005f, 0.005f },
	glm::vec3{ 0.05f, 0.05f, 0.05f },
	glm::vec3{ 0.3f, 0.3f, 0.3f },
};

GLErrorHandler err{};

//glm::mat4 lightRotation{};
float lightRotation{};

SpotLight flashLight{
	camera.getPosition(),
	camera.getFront(),
	glm::vec3{ 0.1f, 0.1f, 0.1f },
	glm::vec3{ 0.6f, 0.6f, 0.6f },
	glm::vec3{ 0.92f, 0.92f, 0.92f },
	1.0f, 0.45f, 0.075f,
	(float) glm::cos(glm::radians(12.5f)),
	(float) glm::cos(glm::radians(17.5f))
};

glm::vec3 lightDiffuseColor{ Constants::pointLights[0].getDiffuse() };
glm::vec3 lightSpecularColor{ Constants::pointLights[0].getSpecular() };

ImGuiIO* io;

int main(int argc, char* argv[]) {
#ifdef _DEBUG_
	std::cout << "Running in debug mode." << std::endl;
#endif

	glfwSetErrorCallback(errorCallback);

	if (!glfwInit()) {
		std::cout << "Failed to initialize GLFW!" << std::endl;
		return -1;
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_SAMPLES, 4);

	GLFWwindow* window{ glfwCreateWindow(screenWidth, screenHeight, "HDR Rendering", nullptr, nullptr) };

	if (window == nullptr) {
		std::cout << "Failed to create GLFW window!" << std::endl;
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
		std::cout << "Failed to initialize GLAD!" << std::endl;
		glfwTerminate();
		return -1;
	}

	glViewport(0, 0, screenWidth, screenHeight);

	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	glfwSetCursorPosCallback(window, mouseMoveCallback);
	glfwSetScrollCallback(window, mouseScrollCallback);
	glfwSetCharCallback(window, characterCallback);

	//glfwSwapInterval(1);

	glfwSetFramebufferSizeCallback(window, frameBufferSizeCallback);

	GLFWwindow* uiWindow{ glfwCreateWindow(configPanelWidth, configPanelHeight, "Config Panel", nullptr, nullptr) };

	if (uiWindow == nullptr) {
		std::cout << "Failed to create GLFW ui window!" << std::endl;
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(uiWindow);
	glfwSetWindowPos(uiWindow, screenWidth * 1.175f, (int)(screenHeight / 3.0f));

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
		std::cout << "Failed to initialize GLAD!" << std::endl;
		glfwTerminate();
		return -1;
	}

	IMGUI_CHECKVERSION();
	ImGui::CreateContext();

	io = &ImGui::GetIO(); (void)io;

	ImGui_ImplGlfw_InitForOpenGL(uiWindow, true);
	ImGui_ImplOpenGL3_Init(Constants::glslVersion);

	ImGui::StyleColorsDark();

	glViewport(0, 0, configPanelWidth, configPanelHeight);

	glfwMakeContextCurrent(window);

	try {
		std::string vShaderFilePath{ appBasePath }, fShaderFilePath{ appBasePath };
		vShaderFilePath += "Shader\\DefaultShader.vert";
		fShaderFilePath += "Shader\\DefaultShader.frag";

		defaultShader = Shader{
			vShaderFilePath.c_str(),
			fShaderFilePath.c_str()
		};

		fShaderFilePath = appBasePath;

		fShaderFilePath += "Shader\\AlphaTexture.frag";

		alphaShader = Shader{
			vShaderFilePath.c_str(),
			fShaderFilePath.c_str()
		};

		vShaderFilePath = appBasePath;
		fShaderFilePath = appBasePath;

		vShaderFilePath += "Shader\\LightSource.vert";
		fShaderFilePath += "Shader\\LightSource.frag";

		lightSourceShader = Shader{
			vShaderFilePath.c_str(),
			fShaderFilePath.c_str()
		};

		vShaderFilePath = appBasePath;
		fShaderFilePath = appBasePath;

		vShaderFilePath += "Shader\\Skybox.vert";
		fShaderFilePath += "Shader\\Skybox.frag";

		skyboxShader = Shader{
			vShaderFilePath.c_str(),
			fShaderFilePath.c_str()
		};

		vShaderFilePath = appBasePath;
		fShaderFilePath = appBasePath;

		vShaderFilePath += "Shader\\SimpleDepth.vert";
		fShaderFilePath += "Shader\\SimpleDepth.frag";

		simpleDepth = Shader{
			vShaderFilePath.c_str(),
			fShaderFilePath.c_str()
		};

		vShaderFilePath = appBasePath;
		fShaderFilePath = appBasePath;

		vShaderFilePath += "Shader\\HDR.vert";
		fShaderFilePath += "Shader\\HDR.frag";

		hdrShader = Shader{
			vShaderFilePath.c_str(),
			fShaderFilePath.c_str()
		};

		vShaderFilePath = appBasePath;
		fShaderFilePath = appBasePath;
		std::string gShaderFilePath{ appBasePath };

		vShaderFilePath += "Shader\\OmnidirectionalShadow.vert";
		fShaderFilePath += "Shader\\OmnidirectionalShadow.frag";
		gShaderFilePath += "Shader\\OmnidirectionalShadow.geom";

		omnidirectionalShadow = Shader{
			vShaderFilePath.c_str(),
			fShaderFilePath.c_str(),
			gShaderFilePath.c_str()
		};

		for(auto& pointLight : Constants::pointLights)
			pointLight.initialize();

		defaultShader.use();
		defaultShader.setDirectionalLight("globalLightSource", directionalLight);
		defaultShader.setMatrix("lightSpace", directionalLight.getProjectionMatrix() * directionalLight.getViewMatrix());
		defaultShader.setMaterial("material", material);
		defaultShader.setInt("skybox", 5);
		defaultShader.setInt("shadowMap", 6);
		defaultShader.setPointLights("pointLights", Constants::pointLights);
		defaultShader.validate();

		skyboxShader.use();
		skyboxShader.setInt("skybox", 3);
		skyboxShader.setFloat("gamma", gamma);
		skyboxShader.validate();

		lightSourceShader.use();
		lightSourceShader.setFloat("gamma", gamma);

		simpleDepth.use();
		simpleDepth.setMatrix("lightSpace", directionalLight.getProjectionMatrix() * directionalLight.getViewMatrix());
		simpleDepth.validate();

		omnidirectionalShadow.use();
		omnidirectionalShadow.validate();

		hdrShader.use();
		hdrShader.setInt("hdrColorBuffer", 0);
		hdrShader.setFloat("gamma", gamma);
		hdrShader.setInt("useGammaCorrection", 1);
		hdrShader.setInt("postProcessingMode", (int) postProcessingMode);
		hdrShader.validate();
	}
	catch (std::exception& e) {
		std::cout << e.what() << std::endl;
		std::cout << "ERROR: Fatal exception. Terminating in 5 seconds .." << std::endl;
		std::this_thread::sleep_for(std::chrono::duration<int, std::milli>{5000});
		return -1;
	}

	glGenVertexArrays(1, &cubeVAO);
	glGenBuffers(1, &vertexBuffer);
	glGenBuffers(1, &elementBuffer);

	glBindVertexArray(cubeVAO);

	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertexData), vertexData, GL_STATIC_DRAW);

	setVertexAttributePointers();

	glGenVertexArrays(1, &lightSourceVAO);

	glBindVertexArray(lightSourceVAO);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);

	glVertexAttribPointer(
		Constants::VertexAttributeLocation::Position,
		3,
		GL_FLOAT,
		GL_FALSE,
		sizeof(float) * 11,
		(void*) nullptr
	);
	glEnableVertexAttribArray(Constants::VertexAttributeLocation::Position);

	glGenVertexArrays(1, &floorVertexArray);
	glBindVertexArray(floorVertexArray);

	glGenBuffers(1, &floorVertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, floorVertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Constants::floorVertices), Constants::floorVertices, GL_STATIC_DRAW);

	setVertexAttributePointers();

	glGenVertexArrays(1, &fireVertexArray);
	glBindVertexArray(fireVertexArray);

	glGenBuffers(1, &fireVertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, fireVertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(Constants::fireVertices), Constants::fireVertices, GL_STATIC_DRAW);

	setVertexAttributePointers();

	glGenVertexArrays(1, &skyboxVertexArray);
	glBindVertexArray(skyboxVertexArray);

	glGenBuffers(1, &skyboxVertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, skyboxVertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), skyboxVertices, GL_STATIC_DRAW);

	glVertexAttribPointer(
		Constants::VertexAttributeLocation::Position,
		3,
		GL_FLOAT,
		GL_FALSE,
		3 * sizeof(float),
		(void*) nullptr
	);
	glEnableVertexAttribArray(Constants::VertexAttributeLocation::Position);

	glGenVertexArrays(1, &screenVAO);
	glGenBuffers(1, &screenVBO);

	glBindVertexArray(screenVAO);
	glBindBuffer(GL_ARRAY_BUFFER, screenVBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(screenVertices), screenVertices, GL_STATIC_DRAW);

	glVertexAttribPointer(
		Constants::VertexAttributeLocation::Position,
		3,
		GL_FLOAT,
		GL_FALSE,
		sizeof(float) * 5,
		(void*) nullptr
	);
	glEnableVertexAttribArray(Constants::VertexAttributeLocation::Position);

	glVertexAttribPointer(
		Constants::VertexAttributeLocation::Texture,
		2,
		GL_FLOAT,
		GL_FALSE,
		sizeof(float) * 5,
		(void*)(sizeof(float) * 3)
	);
	glEnableVertexAttribArray(Constants::VertexAttributeLocation::Texture);

	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	try {
		cubeDiffuseMap = loadTexture("random_bricks_thick_diff_1k.jpg");
		cubeSpecularMap = loadTexture("random_bricks_thick_spec_1k.jpg");
		cubeNormalMap = loadTexture("random_bricks_thick_nor_1k.jpg");
		cubeRoughnessMap = loadTexture("random_bricks_thick_rough_1k.jpg");
		cubeDisplacementMap = loadTexture("random_bricks_thick_disp_1k.jpg");

		floorDiffuseMap = loadTexture("metal\\TexturesCom_Metal_TreadplatePainted2_1K_albedo.jpg");
		floorSpecularMap = loadTexture("metal\\TexturesCom_Metal_TreadplatePainted2_1K_metallic.jpg");
		floorNormalMap = loadTexture("metal\\TexturesCom_Metal_TreadplatePainted2_1K_normal.jpg");
		floorRoughnessMap = loadTexture("metal\\TexturesCom_Metal_TreadplatePainted2_1K_roughness.jpg");
		floorDisplacementMap = loadTexture("metal\\TexturesCom_Metal_TreadplatePainted2_1K_displace.jpg");
	}
	catch (std::exception& e) {
		std::cout << "ERROR: Exception occured while loading textures:" << std::endl
			<< e.what() << std::endl;
	}

	const std::vector<std::string> skyboxTextures = {
		"envmap_interstellar\\interstellar_lf.tga",
		"envmap_interstellar\\interstellar_rt.tga",
		"envmap_interstellar\\interstellar_up.tga",
		"envmap_interstellar\\interstellar_dn.tga",
		"envmap_interstellar\\interstellar_ft.tga",
		"envmap_interstellar\\interstellar_bk.tga",
	};

	skybox = loadCubeMap(skyboxTextures);

	std::pair<unsigned int, unsigned int> frameBufferMsTexturePair{ createFrameBufferMultiSampled() };
	std::pair<unsigned int, unsigned int> frameBufferHDRTexturePair{ createHDRFrameBufferMultisampled() };
	std::pair<unsigned int, unsigned int> frameBufferTexturePair{ createHDRFrameBuffer() };

	multiSampledFBO = frameBufferMsTexturePair.first;
	multiSampledTexture = frameBufferMsTexturePair.second;

	hdrFBO = frameBufferHDRTexturePair.first;
	hdrTexture = frameBufferHDRTexturePair.second;

	frameBuffer = frameBufferTexturePair.first;
	frameBufferTexture = frameBufferTexturePair.second;

	glGenFramebuffers(1, &depthMapFBO);

	glGenTextures(1, &depthMap);
	glBindTexture(GL_TEXTURE_2D, depthMap);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, Constants::depthMapWidth, Constants::depthMapHeight, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
	float borderColor[]{ 1.0f, 1.0f, 1.0f, 1.0f };
	glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);

	glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, depthMap, 0);
	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		std::cout << "ERROR: Failed to complete frame buffer for shadow map." << std::endl;

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	glEnable(GL_MULTISAMPLE);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// uncomment the next line to draw in wireframe mode
	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	float lastFrame{};
	float startTime{ (float) glfwGetTime() };

	try {
		while (!glfwWindowShouldClose(window)) {
			float currentTime{ (float) glfwGetTime() };
			float deltaT{ currentTime - lastFrame };
			lastFrame = currentTime;
			secondsElapsed = currentTime - startTime;

			glfwMakeContextCurrent(window);

			processInput(window, camera, deltaT);

			camera.setSpeed(cameraSpeed);
			updateLights(deltaT);
			renderShadowMap();
			renderShadowCubeMaps();
			//glBindFramebuffer(GL_FRAMEBUFFER, multiSampledFBO);
			//defaultShader.use();
			//defaultShader.setInt("useGammaCorrection", 1);

			//drawScene();

			//glBindFramebuffer(GL_READ_FRAMEBUFFER, multiSampledFBO);
			//glBindFramebuffer(GL_DRAW_FRAMEBUFFER, frameBuffer);
			//glBlitFramebuffer(0, 0, screenWidth, screenHeight, 0, 0, screenWidth, screenHeight, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT, GL_NEAREST);
			//glBindFramebuffer(GL_FRAMEBUFFER, 0);

			glBindFramebuffer(GL_FRAMEBUFFER, hdrFBO);

			drawScene(true);

			glBindFramebuffer(GL_READ_FRAMEBUFFER, hdrFBO);
			glBindFramebuffer(GL_DRAW_FRAMEBUFFER, frameBuffer);
			glBlitFramebuffer(0, 0, screenWidth, screenHeight, 0, 0, screenWidth, screenHeight, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT, GL_NEAREST);
			glBindFramebuffer(GL_FRAMEBUFFER, 0);

			drawHDRTexture();

			glfwPollEvents();
			glfwSwapBuffers(window);

			glfwMakeContextCurrent(uiWindow);
			glfwGetFramebufferSize(uiWindow, &configPanelWidth, &configPanelHeight);

			glViewport(0, 0, configPanelWidth, configPanelHeight);
			glClearColor(0.2f, 0.2f, 0.4f, 1.0f);
			glClear(GL_COLOR_BUFFER_BIT);

			drawUserInterface(deltaT);
			ImGui::Render();
			ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
			
			glfwSwapBuffers(uiWindow);
			glfwPollEvents();

			++frameCount;
		}
	}
	catch (std::exception& e) {
		std::cout << "ERROR: Exception caught: " << std::endl << e.what() << std::endl;
	}
	catch (...) {
		std::cout << "ERROR: Fatal Exception occured. Terminating .. " << std::endl;
	}

	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();

	glDeleteVertexArrays(1, &cubeVAO);
	glDeleteVertexArrays(1, &lightSourceVAO);
	glDeleteBuffers(1, &elementBuffer);
	glDeleteBuffers(1, &vertexBuffer);
	glfwTerminate();

	return 0;
}

void drawScene(bool useOffScreenBuffer) {
	GLErrorHandler err{};
	glViewport(0, 0, screenWidth, screenHeight);
	glClearColor(0.2f, 0.1f, 0.35f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	err.throwOnError();
	defaultShader.use();

	defaultShader.setFloat("parallaxHeightScale", parallaxHeightScale);
	defaultShader.setFloat("parallaxMinLayers", (float) parallaxMinLayers);
	defaultShader.setFloat("parallaxMaxLayers", (float) parallaxMaxLayers);
	defaultShader.setVector("viewPosition", camera.getPosition());
	defaultShader.setMatrix("view", camera.getViewMatrix());
	defaultShader.setMatrix("projection", camera.getProjectionMatrix());
	defaultShader.setMatrix("lightSpace", directionalLight.getProjectionMatrix() * directionalLight.getViewMatrix());

	flashLight.position = camera.getPosition();
	flashLight.direction = camera.getFront();

	defaultShader.setMaterial("material", material);

	defaultShader.setSpotLight("flashLight", flashLight);
	defaultShader.setPointLights("pointLights", Constants::pointLights);

	drawRoom();

	drawCubes(defaultShader);

	lightSourceShader.use();
	lightSourceShader.setFloat("gamma", gamma);

	glBindVertexArray(lightSourceVAO);

	for (int i = 0; i < Constants::pointLights.size(); ++i) {
		glm::mat4 model{ glm::translate(glm::mat4{}, Constants::pointLights[i].getPosition()) };
		if(i > 0)
			model = glm::rotate(model, Constants::rotations[i], Constants::rotationAxis[i]);

		lightSourceShader.setMatrix("model", model);
		lightSourceShader.setMatrix("view", camera.getViewMatrix());
		lightSourceShader.setMatrix("projection", camera.getProjectionMatrix());

		lightSourceShader.setVector("lightSourceColor", Constants::pointLights[i].getDiffuse());

		glDrawArrays(GL_TRIANGLES, 0, 36);
	}
	

	skyboxShader.use();
	skyboxShader.setFloat("gamma", gamma);
	skyboxShader.setMatrix("view", glm::mat3{ camera.getViewMatrix() });
	skyboxShader.setMatrix("projection", camera.getProjectionMatrix());

	glDepthFunc(GL_LEQUAL);

	glBindVertexArray(skyboxVertexArray);
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_CUBE_MAP, skybox);

	glDrawArrays(GL_TRIANGLES, 0, 36);

	glDepthFunc(GL_LESS);
	err.throwOnError();
};

void drawCubes(Shader& shader) {
	GLErrorHandler err{};
	glBindVertexArray(cubeVAO);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, cubeDiffuseMap);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, cubeSpecularMap);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, cubeNormalMap);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, cubeRoughnessMap);

	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, cubeDisplacementMap);

	glActiveTexture(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_CUBE_MAP, skybox);

	glActiveTexture(GL_TEXTURE6);
	glBindTexture(GL_TEXTURE_2D, depthMap);

	for (int i = 0; i < Constants::pointLights.size(); ++i) {
		glActiveTexture(GL_TEXTURE0 + Constants::pointLights[i].getTextureUnit());
		glBindTexture(GL_TEXTURE_CUBE_MAP, Constants::pointLights[i].getDepthMapTexture());
	};

	shader.use();
	shader.setFloat("material.shininess", 256.0f);

	for (int i = 0; i < 9; ++i) {
		glm::mat4 model{ glm::translate(glm::mat4{}, cubePositions[i]) };

		glm::mat3 normalMatrix{ glm::transpose(glm::inverse(glm::mat3 { model })) };

		shader.setMatrix("model", model);
		shader.setMatrix("normalMatrix", normalMatrix);

		glDrawArrays(GL_TRIANGLES, 0, 36);
	}
};

void drawRoom() {
	GLErrorHandler err{};

	glBindVertexArray(floorVertexArray);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, floorDiffuseMap);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, floorSpecularMap);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, floorNormalMap);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, floorRoughnessMap);

	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, floorDisplacementMap);

	glActiveTexture(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_CUBE_MAP, skybox);

	glActiveTexture(GL_TEXTURE6);
	glBindTexture(GL_TEXTURE_2D, depthMap);

	for (int i = 0; i < Constants::pointLights.size(); ++i) {
		glActiveTexture(GL_TEXTURE0 + Constants::pointLights[i].getTextureUnit());
		glBindTexture(GL_TEXTURE_CUBE_MAP, Constants::pointLights[i].getDepthMapTexture());
	};

	defaultShader.use();

	glm::vec3 position{ Constants::floorPosition };
	glm::mat4 identity{};

	glm::mat4 model{ glm::scale(identity, glm::vec3{ 15.0f, 1.0f, 15.0f }) };
	model = glm::translate(model, position);

	glm::mat3 normalMatrix{ glm::transpose(glm::inverse(glm::mat3 { model })) };

	defaultShader.setMatrix("model", model);
	defaultShader.setMatrix("normalMatrix", glm::mat3{});

	glDrawArrays(GL_TRIANGLES, 0, 6);

	position += glm::vec3{ 2.0f, 2.0f, 0.0f } * 7.5f;
	model = glm::translate(identity, position);
	model = glm::rotate(model, glm::radians(90.0f), glm::vec3{ 0.0f, 0.0f, 1.0f });
	model = glm::scale(model, glm::vec3{ 15.0f, 1.0f, 15.0f });

	normalMatrix = glm::transpose(glm::inverse(glm::mat3 { model }));

	defaultShader.setMatrix("model", model);
	defaultShader.setMatrix("normalMatrix", glm::mat3{});

	glDrawArrays(GL_TRIANGLES, 0, 6);

	position += glm::vec3{ -2.0f, 2.0f, 0.0f } * 7.5f;
	model = glm::translate(identity, position);
	model = glm::rotate(model, glm::radians(180.0f), glm::vec3{ 0.0f, 0.0f, 1.0f });
	model = glm::scale(model, glm::vec3{ 15.0f, 1.0f, 15.0f });

	normalMatrix = glm::transpose(glm::inverse(glm::mat3{ model }));

	defaultShader.setMatrix("model", model);
	defaultShader.setMatrix("normalMatrix", glm::mat3{});

	glDrawArrays(GL_TRIANGLES, 0, 6);

	position += glm::vec3{ -2.0f, -2.0f, 0.0f } * 7.5f;
	model = glm::translate(identity, position);
	model = glm::rotate(model, glm::radians(270.0f), glm::vec3{ 0.0f, 0.0f, 1.0f });
	model = glm::scale(model, glm::vec3{ 15.0f, 1.0f, 15.0f });

	normalMatrix = glm::transpose(glm::inverse(glm::mat3{ model }));

	defaultShader.setMatrix("model", model);
	defaultShader.setMatrix("normalMatrix", glm::mat3{});

	glDrawArrays(GL_TRIANGLES, 0, 6);

	position += glm::vec3{ 2.0f, 0.0f, 2.0f } * 7.5f;
	model = glm::translate(identity, position);
	model = glm::rotate(model, glm::radians(90.0f), glm::vec3{ 1.0f, 0.0f, 0.0f });
	model = glm::scale(model, glm::vec3{ 15.0f, 1.0f, 15.0f });

	normalMatrix = glm::transpose(glm::inverse(glm::mat3{ model }));

	defaultShader.setMatrix("model", model);
	defaultShader.setMatrix("normalMatrix", glm::mat3{});

	glDrawArrays(GL_TRIANGLES, 0, 6);

	position += glm::vec3{ 0.0f, 0.0f, -4.0f } * 7.5f;
	model = glm::translate(identity, position);
	model = glm::rotate(model, glm::radians(270.0f), glm::vec3{ 1.0f, 0.0f, 0.0f });
	model = glm::scale(model, glm::vec3{ 15.0f, 1.0f, 15.0f });

	normalMatrix = glm::transpose(glm::inverse(glm::mat3{ model }));

	defaultShader.setMatrix("model", model);
	defaultShader.setMatrix("normalMatrix", glm::mat3{});

	glDrawArrays(GL_TRIANGLES, 0, 6);
};

void drawHDRTexture() {
	glDisable(GL_BLEND);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	hdrShader.use();
	hdrShader.setFloat("gamma", gamma);
	hdrShader.setFloat("exposure", exposure);
	hdrShader.setInt("postProcessingMode", (int)postProcessingMode);

	glBindVertexArray(screenVAO);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, frameBufferTexture);

	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

	glEnable(GL_BLEND);
};

void drawUserInterface(float deltaT) {
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	ImGui::NewFrame();

	const glm::vec4 headerColor{ 0.7f, 0.7f, 0.15f, 1.0f };

	ImGui::Begin("Tool Panel");

	ImGui::BeginGroup();

	ImGui::TextColored(headerColor, "Stat Monitor");
	
	float fps{ (float)frameCount / secondsElapsed };
	ImGui::Text("FPS: %.1f", fps);
	ImGui::Text("Time elapsed: %.3f seconds", secondsElapsed);
	ImGui::Text("FOV: %.2f", camera.getFov());

	ImGui::EndGroup();

	ImGui::NewLine();
	ImGui::TextColored(headerColor, "General");

	ImGui::SliderFloat("Gamma", &gamma, 0.0f, 20.0f);
	ImGui::SliderFloat("Camera speed", &cameraSpeed, 2.5f, 50.0f);

	ImGui::NewLine();
	ImGui::TextColored(headerColor, "Lighting Options");

	ImGui::ColorEdit3("Diffuse color", (float*)&lightDiffuseColor);
	ImGui::ColorEdit3("Specular color", (float*)&lightSpecularColor);
	ImGui::SliderFloat("Exposure", &exposure, 0.0f, 10.0f);

	ImGui::NewLine();
	ImGui::TextColored(headerColor, "Post Processing Options");

	if (ImGui::Button("None")) {
		postProcessingMode = Constants::PostProcessing::None;
	}

	ImGui::SameLine();
	if (ImGui::Button("Invert")) {
		postProcessingMode = Constants::PostProcessing::Invert;
	}

	ImGui::SameLine();
	if (ImGui::Button("Greyscale")) {
		postProcessingMode = Constants::PostProcessing::Greyscale;
	}

	if (ImGui::Button("Box Blur")) {
		postProcessingMode = Constants::PostProcessing::Blur;
	}

	ImGui::SameLine();
	if (ImGui::Button("Sharpen")) {
		postProcessingMode = Constants::PostProcessing::Sharpen;
	}

	ImGui::SameLine();
	if (ImGui::Button("Edge Detect")) {
		postProcessingMode = Constants::PostProcessing::EdgeDetect;
	}
		
	ImGui::NewLine();
	ImGui::TextColored(headerColor, "Parallax Mapping Options");
	
	ImGui::SliderFloat("Height Factor", &parallaxHeightScale, -0.5f, 0.5f);
	ImGui::SliderInt("Minimum Layers", &parallaxMinLayers, 1, 64);
	ImGui::SliderInt("Maximum Layers", &parallaxMaxLayers, parallaxMinLayers, parallaxMinLayers * 4);

	

	ImGui::End();
};

void renderImGui() {
	ImGui::Render();
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
};

void renderShadowMap() {
	glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);

	glViewport(0, 0, Constants::depthMapWidth, Constants::depthMapHeight);
	glBindFramebuffer(GL_FRAMEBUFFER, depthMapFBO);
	glClear(GL_DEPTH_BUFFER_BIT);

	drawCubes(simpleDepth);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, screenWidth, screenHeight);

	glCullFace(GL_BACK);
	glDisable(GL_CULL_FACE);
};

void renderShadowCubeMaps(void) {
	glEnable(GL_CULL_FACE);
	
	glViewport(0, 0, Constants::depthMapWidth, Constants::depthMapHeight);

	for (auto& pointLight : Constants::pointLights) {

		glBindFramebuffer(GL_FRAMEBUFFER, pointLight.getCubeMapFramebuffer());
		glClear(GL_DEPTH_BUFFER_BIT);

		omnidirectionalShadow.use();
		omnidirectionalShadow.setMatrices(
			"shadowMatrices",
			pointLight.getLightSpaceMatrices(screenWidth, screenHeight)
		);
		omnidirectionalShadow.setVector("lightPos", pointLight.getPosition());
		omnidirectionalShadow.setFloat("farPlane", pointLight.getFarPlane());

		drawCubes(omnidirectionalShadow);
	};

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, screenWidth, screenHeight);

	glDisable(GL_CULL_FACE);
};


void updateLights(float deltaT) {
	Constants::pointLights[0].setDiffuse(lightDiffuseColor);
	Constants::pointLights[0].setSpecular(lightSpecularColor);

	//for (int i = 1; i < Constants::pointLights.size(); ++i) {
	//	glm::vec3 shift{};
	//	switch (i) {
	//	case 1:
	//		shift = glm::vec3{ glm::cos(glfwGetTime()), 0.0f, glm::sin(glfwGetTime()) };
	//		Constants::rotations[i] += deltaT * 5.05f;
	//		break;
	//	case 2:
	//		shift = glm::vec3{ 0.0f, glm::cos(glfwGetTime()), glm::sin(glfwGetTime()) };
	//		Constants::rotations[i] += deltaT * 2.02f;
	//		break;
	//	case 3:
	//		shift = glm::vec3{ glm::cos(glfwGetTime()), glm::sin(glfwGetTime()), 0.0f };
	//		Constants::rotations[i] += deltaT * 3.08f;
	//		break;
	//	default:
	//		break;
	//	};
	//		
	//	Constants::pointLights[i].setPosition(Constants::rotationCenters[i] + shift * Constants::radii[i] );
	//};
};

void setVertexAttributePointers() {
	glVertexAttribPointer(
		Constants::VertexAttributeLocation::Position,
		3,
		GL_FLOAT,
		GL_FALSE,
		11 * sizeof(float),
		(void*) nullptr
	);
	glEnableVertexAttribArray(Constants::VertexAttributeLocation::Position);

	glVertexAttribPointer(
		Constants::VertexAttributeLocation::Normal,
		3,
		GL_FLOAT,
		GL_FALSE,
		11 * sizeof(float),
		(void*)(sizeof(float) * 3)
	);
	glEnableVertexAttribArray(Constants::VertexAttributeLocation::Normal);

	glVertexAttribPointer(
		Constants::VertexAttributeLocation::Texture,
		2,
		GL_FLOAT,
		GL_FALSE,
		11 * sizeof(float),
		(void*)(sizeof(float) * 6)
	);
	glEnableVertexAttribArray(Constants::VertexAttributeLocation::Texture);

	glVertexAttribPointer(
		Constants::VertexAttributeLocation::Tangent,
		3,
		GL_FLOAT,
		GL_FALSE,
		11 * sizeof(float),
		(void*)(sizeof(float) * 8)
	);
	glEnableVertexAttribArray(Constants::VertexAttributeLocation::Tangent);
};

void processInput(GLFWwindow* window, Camera3D& camera, float deltaT) {
	if (window == nullptr)
		return;

	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GLFW_TRUE);
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		camera.moveForward(deltaT);
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
		camera.moveBackward(deltaT);
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
		camera.moveRight(deltaT);
	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
		camera.moveLeft(deltaT);

	if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS |
		glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_REPEAT) {
		glm::vec3 movement{ Constants::pointLights[0].getPosition() };
		movement.x -= deltaT * 2.5f;
		Constants::pointLights[0].setPosition(movement);
	}

	if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS |
		glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_REPEAT) {
		glm::vec3 movement{ Constants::pointLights[0].getPosition() };
		movement.x += deltaT * 4.0f;
		Constants::pointLights[0].setPosition(movement);
	}

	if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS |
		glfwGetKey(window, GLFW_KEY_UP) == GLFW_REPEAT) {
		glm::vec3 movement{ Constants::pointLights[0].getPosition() };
		movement.z += deltaT * 4.0f;
		Constants::pointLights[0].setPosition(movement);
	}

	if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS |
		glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_REPEAT) {
		glm::vec3 movement{ Constants::pointLights[0].getPosition() };
		movement.z -= deltaT * 4.0f;
		Constants::pointLights[0].setPosition(movement);
	}

	if (glfwGetKey(window, GLFW_KEY_KP_8) == GLFW_PRESS |
		glfwGetKey(window, GLFW_KEY_KP_8) == GLFW_REPEAT) {
		glm::vec3 movement{ Constants::pointLights[0].getPosition() };
		movement.y += deltaT * 4.0f;
		Constants::pointLights[0].setPosition(movement);
	}

	if (glfwGetKey(window, GLFW_KEY_KP_2) == GLFW_PRESS |
		glfwGetKey(window, GLFW_KEY_KP_2) == GLFW_REPEAT) {
		glm::vec3 movement{ Constants::pointLights[0].getPosition() };
		movement.y -= deltaT * 4.0f;
		Constants::pointLights[0].setPosition(movement);
	}
}

std::pair<unsigned int, unsigned int> createFrameBuffer() {
	unsigned int frameBuffer{}, colorBuffer{}, depthStencilBuffer{};

	glGenFramebuffers(1, &frameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);

	glGenTextures(1, &colorBuffer);
	glBindTexture(GL_TEXTURE_2D, colorBuffer);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, screenWidth, screenHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glBindTexture(GL_TEXTURE_2D, 0);

	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, colorBuffer, 0);

	glGenRenderbuffers(1, &depthStencilBuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, depthStencilBuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, screenWidth, screenHeight);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);

	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, depthStencilBuffer);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		std::cout << "ERROR: Failed to complete frame buffer #" << frameBuffer << std::endl;

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	return std::pair<unsigned int, unsigned int>{frameBuffer, colorBuffer};
};

std::pair<unsigned int, unsigned int> createFrameBufferMultiSampled() {
	unsigned int frameBuffer{}, colorBuffer{}, depthStencilBuffer{};

	glGenFramebuffers(1, &frameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);

	glGenTextures(1, &colorBuffer);
	glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, colorBuffer);
	glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, 4, GL_RGBA, screenWidth, screenHeight, GL_TRUE);
	 
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D_MULTISAMPLE, colorBuffer, 0);
	glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, 0);

	glGenRenderbuffers(1, &depthStencilBuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, depthStencilBuffer);
	glRenderbufferStorageMultisample(GL_RENDERBUFFER, 4, GL_DEPTH24_STENCIL8, screenWidth, screenHeight);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);

	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, depthStencilBuffer);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		std::cout << "ERROR: Failed to complete msaa frame buffer #" << frameBuffer << std::endl;

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	return std::pair<unsigned int, unsigned int>{frameBuffer, colorBuffer};
};

std::pair<unsigned int, unsigned int> createHDRFrameBufferMultisampled() {
	unsigned int frameBuffer{}, colorBuffer{}, depthBuffer{};

	glGenFramebuffers(1, &frameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);

	glGenTextures(1, &colorBuffer);
	glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, colorBuffer);
	//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, screenWidth, screenHeight, 0, GL_RGBA, GL_FLOAT, nullptr);
	glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, 4, GL_RGBA16F, screenWidth, screenHeight, GL_TRUE);

	glGenRenderbuffers(1, &depthBuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
	//glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, screenWidth, screenHeight);
	glRenderbufferStorageMultisample(GL_RENDERBUFFER, 4, GL_DEPTH_COMPONENT, screenWidth, screenHeight);

	//glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, colorBuffer, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D_MULTISAMPLE, colorBuffer, 0);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		std::cout << "ERROR: Failed to complete hdr frame buffer #" << frameBuffer << std::endl;

	glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, 0);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	return std::pair<unsigned int, unsigned int>{frameBuffer, colorBuffer};
};

std::pair<unsigned int, unsigned int> createHDRFrameBuffer() {
	unsigned int frameBuffer{}, colorBuffer{}, depthBuffer{};

	glGenFramebuffers(1, &frameBuffer);
	glBindFramebuffer(GL_FRAMEBUFFER, frameBuffer);

	glGenTextures(1, &colorBuffer);
	glBindTexture(GL_TEXTURE_2D, colorBuffer);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA16F, screenWidth, screenHeight, 0, GL_RGBA, GL_FLOAT, nullptr);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glGenRenderbuffers(1, &depthBuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, depthBuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, screenWidth, screenHeight);

	//glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, colorBuffer, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, colorBuffer, 0);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthBuffer);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
		std::cout << "ERROR: Failed to complete hdr frame buffer #" << frameBuffer << std::endl;

	glBindTexture(GL_TEXTURE_2D, 0);
	glBindRenderbuffer(GL_RENDERBUFFER, 0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	return std::pair<unsigned int, unsigned int>{frameBuffer, colorBuffer};
};

unsigned int loadCubeMap(const std::vector<std::string>& textureFiles) {
	unsigned int cubeMap{};

	if (textureFiles.size() != 6) {
		std::cout << "ERROR: invalid number of cubemap faces (" << textureFiles.size() << ")" << std::endl;
		return -1;
	}

	glGenTextures(1, &cubeMap);
	glBindTexture(GL_TEXTURE_CUBE_MAP, cubeMap);

	for (int i = 0; i < 6; i++) {
		std::string textureFilePath;

		textureFilePath = appBasePath;
		textureFilePath += "Resources\\" + textureFiles[i];

		int width{}, height{}, channels{};
		GLenum format{ GL_RGB }, formatInternal{ GL_SRGB };

		unsigned char* imageBytes = stbi_load(
			textureFilePath.c_str(),
			&width, &height, &channels, 0
		);

		if (imageBytes == nullptr) {
			std::cout << "ERROR: Failed to load texture #" << i << " for cube map." << std::endl;
			std::cout << "\tFilepath: " << textureFilePath << std::endl;
			return -1;
		}

		if (channels == 1) {
			format = GL_RED;
			formatInternal = format;
		}
		if (channels == 3) {
			format = GL_RGB;
			formatInternal = GL_SRGB;
		}
		else if (channels == 4) {
			format = GL_RGBA;
			formatInternal = GL_SRGB_ALPHA;
		}

		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, formatInternal, width, height, 0, format, GL_UNSIGNED_BYTE, imageBytes);

		stbi_image_free(imageBytes);
	};

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	return cubeMap;
};

unsigned int loadTexture(const std::string& textureFile) {
	unsigned int texture{};
	int width{}, height{}, channels{};
	GLenum format{ GL_RGB }, formatInternal{ GL_SRGB };

	std::string textureFilePath;

	textureFilePath = appBasePath;
	textureFilePath += "Resources\\" + textureFile;

	unsigned char* imageBytes = stbi_load(
		textureFilePath.c_str(),
		&width, &height, &channels, 0
	);

	if (imageBytes == nullptr) {
		std::cout << "ERROR: Failed to load the texture image!" << std::endl;
		return -1;
	}

	if (channels == 1) {
		format = GL_RED;
		formatInternal = format;
	}
	if (channels == 3) {
		format = GL_RGB;
		formatInternal = GL_SRGB;
	}
	else if (channels == 4) {
		format = GL_RGBA;
		formatInternal = GL_SRGB_ALPHA;
	}
		
	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexImage2D(GL_TEXTURE_2D, 0, formatInternal, width, height, 0, format, GL_UNSIGNED_BYTE, imageBytes);

	stbi_image_free(imageBytes);

	glGenerateMipmap(GL_TEXTURE_2D);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	if (format == GL_RGBA) {
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	}

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	return texture;
}

void frameBufferSizeCallback(GLFWwindow* window, int width, int height) {
	glViewport(0, 0, width, height);
	screenWidth = width;
	screenHeight = height;
};

void mouseMoveCallback(GLFWwindow* window, double xPos, double yPos) {
	static bool firstCall{ true };
	static double lastX{};
	static double lastY{};

	if (firstCall) {
		lastX = xPos;
		lastY = yPos;
		firstCall = false; 
		return;
	}

	float offsetX{ (float) (xPos - lastX) };
	float offsetY{ (float) (lastY - yPos) };

	lastX = xPos;
	lastY = yPos;

	camera.processMouseMovement(offsetX, offsetY);
};

void mouseScrollCallback(GLFWwindow* window, double offsetX, double offsetY) {
	if (window == nullptr)
		return;

	camera.processMouseScroll((float) offsetY);
};

void characterCallback(GLFWwindow* window, unsigned int codepoint) {
	const char ascii{ (char) codepoint };
	switch (ascii) {
	default:
		break;
	};
};

void errorCallback(int err, const char* msg) {
	std::cout << "ERROR: GLFW error code %d" << std::endl
		<< "ERROR: " << msg << std::endl;
};