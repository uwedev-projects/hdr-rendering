#include "DirectionalLight.h"

#include <glm/gtc/matrix_transform.hpp>

DirectionalLight::DirectionalLight(const glm::vec3& direction, const glm::vec3& ambient, const glm::vec3& diffuse, const glm::vec3& specular)
	:direction{ glm::normalize(direction) }, ambient{ ambient }, diffuse{ diffuse }, specular{ specular } {}

glm::mat4 DirectionalLight::getViewMatrix() const {
	return glm::lookAt(-20.0f * direction, glm::vec3{ 0.0f }, glm::vec3{ 0.0f, 1.0f, 0.0f });
};

glm::mat4 DirectionalLight::getProjectionMatrix() const {
	return glm::ortho(-20.0f, 20.0f, -20.0f, 20.0f, 1.0f, 70.0f);
};

const glm::vec3& DirectionalLight::getDirection() const {
	return direction;
};

const glm::vec3& DirectionalLight::getAmbient() const {
	return ambient;
};

const glm::vec3& DirectionalLight::getDiffuse() const {
	return diffuse;
};

const glm::vec3& DirectionalLight::getSpecular() const {
	return specular;
};

void DirectionalLight::setDirection(const glm::vec3& direction) {
	this->direction = glm::normalize(direction);
};