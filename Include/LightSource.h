#pragma once

#include <glm/glm.hpp>
#include <glm/vec3.hpp>

//struct PointLight {
//	static constexpr float DefaultConstant{ 1.0f };
//	static constexpr float DefaultLinear{ 0.22f };
//	static constexpr float DefaultQuadratic{ 0.2f };
//
//	glm::vec3 position;
//	glm::vec3 ambient;
//	glm::vec3 diffuse;
//	glm::vec3 specular;
//
//	float constant;
//	float linear;
//	float quadratic;
//
//	PointLight(const glm::vec3& position, const glm::vec3& ambient, const glm::vec3& diffuse, const glm::vec3& specular)
//		:	position{ position }, ambient{ ambient }, diffuse{ diffuse }, specular{ specular }, 
//		constant{ DefaultConstant }, linear{ DefaultLinear }, quadratic{ DefaultQuadratic } {}
//
//	PointLight(const glm::vec3& position, const glm::vec3& ambient, const glm::vec3& diffuse, const glm::vec3& specular,
//			   float constant, float linear, float quadratic)
//		: position{ position }, ambient{ ambient }, diffuse{ diffuse }, specular{ specular },
//		constant{ constant }, linear{ linear }, quadratic{ quadratic } {}
//
//	~PointLight() {}
//};

//struct DirectionalLight {
//	glm::vec3 direction;
//	glm::vec3 ambient;
//	glm::vec3 diffuse;
//	glm::vec3 specular;
//
//	DirectionalLight(const glm::vec3& direction, const glm::vec3& ambient, const glm::vec3& diffuse, const glm::vec3& specular)
//		:direction{ glm::normalize(direction) }, ambient{ ambient }, diffuse{ diffuse }, specular{ specular } {}
//
//	~DirectionalLight() {}
//};

struct SpotLight {
	static constexpr float DefaultConstant{ 1.0f };
	static constexpr float DefaultLinear{ 0.09f };
	static constexpr float DefaultQuadratic{ 0.032f };

	glm::vec3 position;
	glm::vec3 direction;

	glm::vec3 ambient;
	glm::vec3 diffuse;
	glm::vec3 specular;

	float constant;
	float linear;
	float quadratic;

	float cutOff;
	float outerCutOff;

	SpotLight(const glm::vec3& position, const glm::vec3& direction, const glm::vec3& ambient, 
			  const glm::vec3& diffuse, const glm::vec3& specular, float cutOff, float outerCutOff)
		: position{ position }, direction{ glm::normalize(direction) }, ambient{ ambient }, diffuse{ diffuse }, specular{ specular },
		constant{ DefaultConstant }, linear{ DefaultLinear }, quadratic{ DefaultQuadratic }, cutOff{ cutOff }, outerCutOff{ outerCutOff } {}

	SpotLight(const glm::vec3& position, const glm::vec3& direction, const glm::vec3& ambient,
		const glm::vec3& diffuse, const glm::vec3& specular, float constant, float linear, float quadratic, 
		float cutOff, float outerCutOff)
		: position{ position }, direction{ glm::normalize(direction) }, ambient{ ambient }, diffuse{ diffuse }, specular{ specular },
		constant{ constant }, linear{ linear }, quadratic{ quadratic }, cutOff{ cutOff }, outerCutOff{ outerCutOff }{}

	~SpotLight() {}
};