#pragma once

#include <glm/glm.hpp>
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>

#include <vector>


class PointLight {
protected:
	static constexpr float DefaultConstant{ 1.0f };
	static constexpr float DefaultLinear{ 0.22f };
	static constexpr float DefaultQuadratic{ 0.2f };

	static constexpr float DefaultNear{ 1.0f };
	static constexpr float DefaultFar{ 35.0f };

	glm::vec3 position;
	glm::vec3 ambient;
	glm::vec3 diffuse;
	glm::vec3 specular;

	float constant;
	float linear;
	float quadratic;

	unsigned int depthMapFBO;
	unsigned int depthMap;
	unsigned int texUnit;

public:
	PointLight(const glm::vec3& position, const glm::vec3& ambient, const glm::vec3& diffuse, const glm::vec3& specular);

	PointLight(const glm::vec3& position, const glm::vec3& ambient, const glm::vec3& diffuse, const glm::vec3& specular,
		float constant, float linear, float quadratic, unsigned int texUnit);

	~PointLight() {}

	void initialize();

	std::vector<glm::mat4> getLightSpaceMatrices(unsigned int screenWidth, unsigned int screenHeight) const;

	unsigned int getCubeMapFramebuffer() const noexcept;
	unsigned int getDepthMapTexture() const noexcept;

	float getFarPlane() const noexcept;

	const glm::vec3& getPosition() const noexcept;
	const glm::vec3& getAmbient() const noexcept;
	const glm::vec3& getDiffuse() const noexcept;
	const glm::vec3& getSpecular() const noexcept;

	float getConstant() const noexcept;
	float getLinear() const noexcept;
	float getQuadratic() const noexcept;

	unsigned int getTextureUnit() const noexcept;

	void setPosition(const glm::vec3& pos);
	void setDiffuse(const glm::vec3& diff);
	void setSpecular(const glm::vec3& spec);
};