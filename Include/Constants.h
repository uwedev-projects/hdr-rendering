#pragma once

#include <glm/glm.hpp>
#include <glm/vec3.hpp>
#include <vector>
#include "Material.h"
#include "PointLight.h"

namespace Constants {
	enum VertexAttributeLocation : unsigned int { 
		Position = 0, 
		Normal = 1,
		Texture = 2,
		Tangent = 3
	};

	enum PostProcessing : int {
		None = 0,
		Invert = 1,
		Greyscale = 2,
		Blur = 3,
		Sharpen = 4,
		EdgeDetect = 5
	};

	constexpr unsigned int screenWidth{ 1280 };
	constexpr unsigned int screenHeight{ 720 };

	constexpr int configPanelWidth{ 400 };
	constexpr int configPanelHeight{ 800 };

	constexpr unsigned int depthMapWidth{ 1024 };
	constexpr unsigned int depthMapHeight{ 1024 };

	extern const char* glslVersion;

	extern std::vector<PointLight> pointLights;
	//extern PointLight pointLight;
	extern glm::vec3 rotationCenters[];
	extern float radii[];

	extern glm::vec3 rotationAxis[];
	extern float rotations[];

	extern float floorVertices[66];
	extern glm::vec3 floorPosition;

	extern float fireVertices[66];
}