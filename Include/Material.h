#pragma once

#include <glm/glm.hpp>
#include <glm/vec3.hpp>

struct Material {
	unsigned int diffuseMap;
	unsigned int specularMap;
	unsigned int normalMap;
	unsigned int roughnessMap;
	unsigned int displacementMap;
	float shininess;

	Material() {}
	Material(
		unsigned int diffuseMap, unsigned int specularMap, unsigned int normalMap,
		unsigned int roughnessMap, unsigned int displacementMap, float shininess
	) :diffuseMap{ diffuseMap }, specularMap{ specularMap }, normalMap{ normalMap }, 
		roughnessMap{ roughnessMap }, displacementMap{ displacementMap }, shininess{ shininess }
	{}

	~Material() {}
};