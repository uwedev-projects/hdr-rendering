#pragma once

#include <glm/glm.hpp>
#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>


class DirectionalLight {
protected:
	glm::vec3 direction;
	glm::vec3 ambient;
	glm::vec3 diffuse;
	glm::vec3 specular;

public:
	DirectionalLight(const glm::vec3& direction, const glm::vec3& ambient, const glm::vec3& diffuse, const glm::vec3& specular);
	~DirectionalLight() {};

	const glm::vec3& getDirection() const;
	const glm::vec3& getAmbient() const;
	const glm::vec3& getDiffuse() const;
	const glm::vec3& getSpecular() const;

	void setDirection(const glm::vec3& direction);

	glm::mat4 getViewMatrix() const;
	glm::mat4 getProjectionMatrix() const;
};