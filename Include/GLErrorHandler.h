#pragma once
#include <glad/glad.h>
#include <string>
#include <unordered_map>
#include <cstddef>
#include <functional>

template <typename E>
struct EnumHasher {
	inline std::size_t operator()(const E& code) const {
		return std::hash<unsigned int>{}(static_cast<unsigned int>(code));
	};
};

class GLErrorHandler
{
protected:
	static const std::unordered_map<GLenum, std::string, EnumHasher<GLenum>> errorMap;
public:
	GLErrorHandler();
	~GLErrorHandler();

	std::string getLastError() const;
	void throwOnError() const;
};

