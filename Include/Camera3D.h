#pragma once

#include <glm/glm.hpp>

class Camera3D
{
protected:
	glm::vec3 position;
	glm::vec3 front;
	glm::vec3 worldUp;
	glm::vec3 right;
	glm::vec3 up;

	float fov;
	float pitch;
	float yaw;
	float speed;
	float sensitivity;

	unsigned int screenWidth;
	unsigned int screenHeight;

	void updateVectors();

public:
	Camera3D();
	Camera3D(const glm::vec3& position, const glm::vec3& front, const glm::vec3& worldUp, float fov = 45.0f, float pitch = 0.0f, float yaw = -90.0f, float sensitivity = .05f);
	~Camera3D();

	const glm::vec3& getPosition() const noexcept;
	const glm::vec3& getFront() const noexcept;
	const glm::vec3& getUp() const noexcept;
	const glm::vec3& getRight() const noexcept;
	float getSpeed() const noexcept;
	float getFov() const noexcept;

	glm::mat4 getViewMatrix() const;
	glm::mat4 getProjectionMatrix() const;

	void setPosition(const glm::vec3 cameraPosition);
	void setTarget(const glm::vec3 target);
	void setFront(const glm::vec3 front);
	void setSpeed(float speed) noexcept;
	void setSensitivity(float sensitivity) noexcept;
	void setScreenDimensions(unsigned int width, unsigned int height) noexcept;

	void moveForward(float deltaT);
	void moveBackward(float deltaT);
	void moveRight(float deltaT);
	void moveLeft(float deltaT);

	void processMouseMovement(float offsetX, float offsetY);
	void processMouseScroll(float offset);
};

